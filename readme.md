# * Git commands

* git init 
* git status
* git branch
* git branch -a
* git branch [new_br_name]
* git branch -d [br_name]
* git push origin -d [remote_br_name]
* git add .
* git commit -m "First commit"
* git commit .
* git remote add origin [git@gitlab.com:programminghero/git-commands.git]
* git push origin [br_name]
* git push --set-upstream origin [new_br_name]
* git checkout [br_name] 
* git checkout -b [new_br_name] 
* git checkout -b [br_name] origin/[remote_br_name] 
* git branch -m [br_name] [rename_br_name]
* git checkout - 
* git checkout -- [undo_file_name.ext]
* git checkout . 
* git merge [br_name] 
* git merge [src_br_name] [tar_br_name] 
* git log
* git stash
* git stash clear